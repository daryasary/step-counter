from django.utils import timezone
from rest_framework.response import Response
from rest_framework.views import APIView


class StaticTimeAPIView(APIView):
    def get(self, request, *args, **kwargs):
        return Response({'time': timezone.now()})
