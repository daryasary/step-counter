from django.conf.urls import url

from configurations.views import StaticTimeAPIView

urlpatterns = [
    url(r'^time/$', StaticTimeAPIView.as_view(), name='time'),
]
