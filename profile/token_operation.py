import uuid
import logging

from django.utils.translation import ugettext as _
from rest_framework import serializers

from .utils import jwt_decode_handler, get_empty_uuid
from .models import UserDevice

logger = logging.getLogger('pyapp.users')


class TokenOperation(object):
    """
    This class contains token validation operation.
    Validate token, store, retrieve and update secret key for token
    Secret key: secret key is a key that embed in token for security(token sniffing attack)

    Attributes:
        __user_id: current user id
        __device_uuid: current user device uuid
    """

    def __init__(self, user_id, device_uuid=get_empty_uuid()):
        self.__user_id = user_id
        self.__device_uuid = device_uuid
        self.__current_user_device = None

    def current_user_device(self):
        """
        in this method retrieve a UserDevice record based on self.__user_id and
        self.__device_uuid and return it

        :returns UserDevice with user_id equals to self.__user_id and device_uuid equals
                 to self.__device_uuid
        """
        if self.__current_user_device:
            return self.__current_user_device

        try:
            self.__current_user_device = UserDevice\
                .objects.select_related('user')\
                .get(user__id=self.__user_id, device_uuid=self.__device_uuid)
            return self.__current_user_device
        except UserDevice.DoesNotExist:
            return None

    @property
    def secret_key(self):
        """
        Retrieve secret key from data store and return it

        :returns UUID: secret key
        """
        current_user_device = self.current_user_device()
        if current_user_device is None:
            logger.error('user_id: {0}, device_uuid: {1}: This device has not been registered yet'
                         .format(self.__user_id, self.__device_uuid))
            msg = _('This device has not been registered yet')
            raise serializers.ValidationError(msg)
        return current_user_device.secret_key

    @secret_key.setter
    def secret_key(self, secret_key):
        """
        Set secret key for UserDevice with user_id equals to self.__user_id
        and device_uuid equals to self.__device_uuid

        :param secret_key: UUID key for generating a valid token

        :raise ValidationError: if UserDevice with user_id equals to self.__user_id
               and device_uuid equals to self.__device_uuid does not exists
        """

        current_user_device = self.current_user_device()
        if current_user_device is None:
            logger.error('user_id: {0}, device_uuid: {1} : This device has not been registered yet'
                         .format(self.__user_id, self.__device_uuid))
            msg = _('This device has not been registered yet')
            raise serializers.ValidationError(msg)

        else:
            current_user_device.secret_key = secret_key
            current_user_device.save()

    @staticmethod
    def get_user_info_from_token(token):
        """
        Decoding the token and extracting user_id and device_uuid from it

        :param token: JWT token

        :returns Int user_id
                 UUID device_uuid

        :raise ValidationError: if payload does not contains user_id key
        """
        payload = jwt_decode_handler(token)
        user_id = payload.get('user_id')
        if user_id is None:
            logger.error('user_id: {0}: The token is invalid'
                         .format(user_id))
            msg = _('The token is invalid')
            raise serializers.ValidationError(msg)

        device_uuid = payload.get('device_uuid')
        return user_id, device_uuid

    def is_token_valid(self, token):
        """
        Checking if the incoming token has a valid secret key or not

        :param token: JWT token

        :returns bool: true if the token is valid else false
        """
        current_user_device = self.current_user_device()
        if current_user_device:
            # checking the incoming token secret key and a secret that store in data store
            secret_key = TokenOperation.get_secret_key(token)
            return uuid.UUID(secret_key) == current_user_device.secret_key

        return False

    @staticmethod
    def get_secret_key(token):
        """
        Getting secret key from the incoming token

        :param token: JWT token

        :returns UUID: a secret key that embed in the token

        :raise ValidationError: if the token does not contains secret key
        """
        payload = jwt_decode_handler(token)
        secret_key = payload.get('secret_key')
        if secret_key is None:
            logger.error('The token does not contain secret_key')
            msg = _('The token is invalid')
            raise serializers.ValidationError(msg)

        return secret_key

    @staticmethod
    def new_secret_key():
        """
        generate new secret key

        :returns UUID: a newly secret key for embedding in token
        """
        return uuid.uuid4()


