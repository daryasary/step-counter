from random import randint
import logging

from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.models import Max
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.validators import UniqueValidator


from .utils import get_encryption_class

from profile.utils import get_verification_function
from .models import UserProfile, UserDevice, UserEmail, UserPhoneNumber
from .settings import pyapp_users_settings
from django.conf import settings
from .mailer import Mailer

User = get_user_model()
logger = logging.getLogger('pyapp.users')


class ProfileSerializer(serializers.ModelSerializer):
    """
    A serializer for user profile that contains username, first_name, ...
    """
    username = serializers.CharField(source='user.username', read_only=True)
    first_name = serializers.CharField(source='user.first_name', required=False, allow_blank=True)
    last_name = serializers.CharField(source='user.last_name', required=False, allow_blank=True)
    email = serializers.CharField(source='user.email', required=False, read_only=True)
    phone_number_v = serializers.IntegerField(source='user.phone_number', required=False, read_only=True)
    step_count = serializers.IntegerField(required=False, read_only=True)

    class Meta:
        model = UserProfile
        exclude = ('id', 'user')
        extra_kwargs = {
            'phone_number': {
                'required': False
            }
        }

    def validate(self, validate_data):
        """
        Checking additional validation for phone_number:
        1. check if phone number already set
        2. check uniqueness of phone number and user
        :param validate_data: a validate_data that contains valid data
        :return validate_data: updated validate_data
        """
        phone_number = validate_data.get('phone_number')
        if phone_number:
            request = self.context['request']
            user = request.user
            if user.phone_number is not None:
                raise serializers.ValidationError(_('Phone Number has already been set.'))
            if User.objects.filter(phone_number=phone_number).exclude(id=user.id).exists():
                raise serializers.ValidationError(_('User with this Phone Number already exists.'))
        return validate_data

    def update(self, instance, validated_data):
        """
        updating the user profile
        :param instance: untouched user instance
        :param validated_data: touched user info
        :return instance: updated user profile instance
        """
        user_data = validated_data.pop('user', None)
        instance = super().update(instance, validated_data)
        if user_data:
            instance.user.first_name = user_data.get('first_name', instance.user.first_name)
            instance.user.last_name = user_data.get('last_name', instance.user.last_name)
            instance.user.save(update_fields=['first_name', 'last_name'])
        return instance

    def get_phone_number(self, obj):
        pass


class UserRegistrationSerializer(serializers.ModelSerializer):
    """
    User registration serializer
    """
    phone_number = serializers.CharField(required=True, write_only=True)

    class Meta:
        model = UserDevice
        fields = ('device_uuid', 'device_model',
                  'device_type', 'notify_token', 'phone_number')

    def create(self, validated_data):
        """
        Override the serializer create method:
        1. sending the incoming data to vas data service for user registration
        2. if response is ok, user registration data will be stored in data store

        :param validated_data: dict -> contains user registration data

        :return UserDevice: the user registration data
        """
        phone_number = validated_data['phone_number']

        # if settings.ENCRYPTION_KEY is None:
        #     raise ValidationError({'message': _('you must set ENCRYPTION_KEY in settings')}, code=400)
        #
        # cipher_class = get_encryption_class()
        # cipher = cipher_class(settings.ENCRYPTION_KEY)

        # try:
        #     phone_number = cipher.decrypt(phone_number)
        # except Exception:
        #     raise ValidationError({'message': _('phone number is invalid')}, code=400)

        if not phone_number:
            raise ValidationError({'message': _('phone number is invalid')}, code=400)

        validated_data.update({'phone_number': phone_number})

        if pyapp_users_settings.VAS_SERVICE_ID == 0:
            return self.sms_registration(validated_data)

        return self.vas_registration(validated_data)

    def sms_registration(self, validated_data):
        """
        ADP user registration
        :param validated_data: user data
        :return UserDevice: the user registration data
        """
        verify_code = str(randint(pyapp_users_settings.VERIFY_CODE_MIN, pyapp_users_settings.VERIFY_CODE_MAX))
        phone_number = validated_data['phone_number']

        try:
            user = User.objects.get(
                phone_number=phone_number
            )
        except User.DoesNotExist:
            user_exists = UserPhoneNumber.objects.filter(old_phone_number=phone_number)
            if user_exists:
                logger.error('check with old_phone_number: {0} phone number already confirm'.format(phone_number))
                raise serializers.ValidationError({'detail': _('Another user with this phone number exists')})
            user = User.objects.create_user(
                phone_number=phone_number
            )
            user.set_verify_code(verify_code)
        else:
            user.set_verify_code(verify_code)
            # user.save(update_fields=['verify_codes'])

        user_device, is_user_device_created = UserDevice.objects.update_or_create(
            user=user,
            device_uuid=validated_data['device_uuid'],
            defaults={
                'notify_token': validated_data.get('notify_token'),
                'device_model': validated_data.get('device_model'),
                'device_os': validated_data.get('device_os'),
                'device_type': validated_data.get('device_type')
            }
        )

        send_verification_func = get_verification_function()
        send_verification_func(phone_number, verify_code)
        return user_device


class UpdateEmailSerializer(serializers.ModelSerializer):
    """
    Adding and substitute user email addresses django rest framework ModelSerializer
    """

    class Meta:
        model = UserEmail
        fields = ('new_email',)

    def create(self, validated_data):
        """
        Override the serializer create method:
        sending the verification email to user

        :param validated_data: dict -> contains user email info

        :return UserEmail: the user email data
        """
        request = self.context['request']
        user = request.user
        old_email = user.email
        new_email = validated_data.get('new_email')

        user_exists = User.objects.filter(email=new_email).first()

        if user.email == new_email or user_exists is not None:
            logger.error('{0} email already confirm'.format(new_email))
            raise serializers.ValidationError({'detail': _('The email already confirmed')})

        user_email = user.emails.filter(new_email=new_email).first()

        if user_email is not None:
            if user_email.is_email_verified():
                logger.error('{0} email already confirm'.format(new_email))
                raise serializers.ValidationError({'detail': _('The email already confirmed')})

        verification_code, verification_url = self.generate_email_verification_url(new_email)

        context = {'verification_email_url': verification_url,
                   'verification_code': verification_code}

        self.send_email_verification(context, new_email)

        if user_email:
            user_email.verify_code = verification_code
            user_email.save()
            return user_email

        return user.emails.create(old_email=old_email, new_email=new_email, verify_code=verification_code)

    def generate_email_verification_url(self, email):
        """
        Generating verification url and verification code for incoming email
        :param email: the new user email
        :returns verification_code: email verification code
                 verification_url: email verification url
        """
        verify_code = randint(pyapp_users_settings.VERIFY_CODE_MIN, pyapp_users_settings.VERIFY_CODE_MAX)
        return verify_code, '{SITE_URL}/{EMAIL_VERIFICATION_URI}/?code={VERIFY_CODE}&email={EMAIL}'\
            .format(SITE_URL=pyapp_users_settings.SITE_URL,
                    EMAIL_VERIFICATION_URI=pyapp_users_settings.EMAIL_VERIFICATION_URI,
                    VERIFY_CODE=verify_code,
                    EMAIL=email)

    def send_email_verification(self, context, to_emails):
        """
        Sending email verification for the incoming message
        sending emails from Django project asynchronously using Django app call djnago-celery-email.
        :param context: verification context for email verification template
        :param to_emails: emails that send the confirmation message to them
        """
        mail = Mailer()
        mail.send_messages(subject=pyapp_users_settings.EMAIL_VERIFICATION_SUBJECT,
                           template=pyapp_users_settings.EMAIL_VERIFICATION_TEMPLATE_PATH,
                           context=context,
                           to_emails=[to_emails])


class UpdatePhoneNumberSerializer(serializers.ModelSerializer):
    """
    Adding and substitute user phone number: django rest framework ModelSerializer
    """

    class Meta:
        model = UserPhoneNumber
        fields = ('new_phone_number', )

    def create(self, validated_data):
        """
        Override the serializer create method:
        sending the verification SMS to user

        :param validated_data: dict -> contains user phone number info

        :return UserPhoneNumber: the user phone number data
        """
        request = self.context['request']
        user = request.user
        logger.info('old user phone number is {0}'.format(user.phone_number))
        if user.phone_number is not None and (pyapp_users_settings.VAS_SERVICE_ID != 0 or not pyapp_users_settings.CAN_CHANGE_PHONE_NUMBER):
            raise serializers.ValidationError({'detail': _('You can not change phone number')})

        if user.phone_number is None and pyapp_users_settings.VAS_SERVICE_ID != 0:
            raise serializers.ValidationError({'detail': _('Your service is VAS and you can not change'
                                                           ' phone number you must use registration')})

        old_phone_number = user.phone_number
        new_phone_number = validated_data.get('new_phone_number')
        user_exists = User.objects.filter(phone_number=new_phone_number).first()

        if user.phone_number == new_phone_number or user_exists is not None:
            logger.error('{0} phone number already confirm'.format(new_phone_number))
            raise serializers.ValidationError({'detail': _('The phone number already confirmed')})

        user_phone_number = user.phone_numbers.filter(new_phone_number=new_phone_number).first()

        if user_phone_number is not None:
            if user_phone_number.is_phone_number_verified():
                logger.error('{0} phone number already confirm'.format(new_phone_number))
                raise serializers.ValidationError({'detail': _('The phone number already confirmed')})

        verification_code = self.generate_phone_number_verification_code()

        send_verification_func = get_verification_function()
        send_verification_func(new_phone_number, verification_code)

        if user_phone_number:
            user_phone_number.verify_code = verification_code
            user_phone_number.save()
            return user_phone_number

        return user.phone_numbers.create(old_phone_number=old_phone_number, new_phone_number=new_phone_number,
                                         verify_code=verification_code)

    def generate_phone_number_verification_code(self):
        """
        Generating verification code for incoming phone number
        :returns verification_code: phone number verification code
        """
        return randint(pyapp_users_settings.VERIFY_CODE_MIN, pyapp_users_settings.VERIFY_CODE_MAX)


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(
            required=False,
            validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
        # extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        with transaction.atomic():
            if 'username' not in validated_data:
                id_max = User.objects.all().aggregate(Max('id'))['id__max']
                next_id = id_max + 1 if id_max else 1
                validated_data['username'] = 'U{}'.format(next_id)
            user = User.objects.create_user(
                username=validated_data['username'],
                email=validated_data['email'],
                password=validated_data['password']
            )
        return user


class UpdateStepSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('step_count',)


class LeaderBoardSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')

    class Meta:
        model = UserProfile
        fields = ('nick_name', 'avatar', 'username', 'step_count')
