from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from rest_framework.permissions import BasePermission


class TimeIsAllowed(BasePermission):
    """Check request is performing in allowed period of time"""
    message = _("Now is not valid time for such a request")

    def has_permission(self, request, view):
        now = timezone.now().time()
        return settings.START_TIME < now < settings.END_TIME
