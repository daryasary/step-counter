from django.conf import settings
from rest_framework.settings import APISettings


USER_SETTINGS = getattr(settings, 'PYAPP_USERS', None)

DEFAULTS = {
    'VAS_SERVICE_ID': 0,
    'VAS_BASE_URL': 'http://vasp.yaramobile.com/api',
    'VAS_REGISTER_URL': '{BASE_URL}/{SERVICE_ID}/devices/',
    'VAS_VERIFY_URL': '{BASE_URL}/verify/{REGISTER_CODE}/{VERIFY_CODE}/',
    'FINOTECH_SERVICE_ID': 0,
    'FINOTECH_TOKEN_URL': '',
    'FINOTECH_VERIFY_URL': '',
    'SITE_URL': 'http://localhost:8000',
    'EMAIL_VERIFICATION_URI': 'auth/manage/user-email/verify',
    'VERIFY_CODE_MIN': 10000,
    'VERIFY_CODE_MAX': 99999,
    'EMAIL_VERIFICATION_TEMPLATE_PATH': None,
    'EMAIL_VERIFICATION_SUBJECT': 'اعتبار سنجی ایمیل',
    'CAN_CHANGE_PHONE_NUMBER': False,
    'SMS_VERIFICATION_HANDLER': 'profile.utils.send_verification',  # Required
    'ENCRYPTION_CLASS': 'profile.utils.AESCipher',
    'ENCRYPTION_KEY': None,  # Required
    'VERIFICATION_URI': 'http://ws.adpdigital.com/url/send',
    'VERIFICATION_TEMPLATE_PATH': 'templates/phone_verify.text'
}

pyapp_users_settings = APISettings(USER_SETTINGS, DEFAULTS)
