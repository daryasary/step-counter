from importlib import import_module
import logging
import requests

from django.core.exceptions import ValidationError, MultipleObjectsReturned, ObjectDoesNotExist
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.conf import settings
from django.core.validators import validate_email
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from oauth2client import client

from .models import UserDevice

from .settings import pyapp_users_settings


User = get_user_model()
logger = logging.getLogger('pyapp.users')
auth_user_settings = getattr(settings, 'AUTH_USER', {})


def migrate_user(device_uuid, user, notify_token=''):
    if auth_user_settings.get('MIGRATE_ANONYMOUS_USERS'):
        try:
            fake_user = User.objects.get(username=device_uuid.hex)
        except (User.DoesNotExist, ValueError) as e:
            pass
        else:
            move_func_str = auth_user_settings.get('MIGRATE_ANONYMOUS_FUNC')
            if move_func_str:
                try:
                    parts = move_func_str.split('.')
                    module_path, class_name = '.'.join(parts[:-1]), parts[-1]
                    module = import_module(module_path)
                    move_user_func = getattr(module, class_name)
                    move_user_func(user, fake_user)
                    logger.info('User Migrate: {0} --> {1}'.format(fake_user.username, user.username))
                except Exception as e:
                    logger.error('User Migrate Error: {0}'.format(e))

    try:
        UserDevice.objects.update_or_create(
            user=user,
            device_uuid=device_uuid,
            defaults={
                'notify_token': notify_token
            }
        )
    except Exception as e:
        logger.error('User Migrate Error: {0}'.format(e))


class SMSBackend(ModelBackend):
    """
    Authenticates against settings.AUTH_USER_MODEL.
    """

    def authenticate(self, username=None, password=None, **kwargs):
        if pyapp_users_settings.VAS_SERVICE_ID is not 0:
            return

        PhoneNumberField = User._meta.get_field('phone_number')
        try:
            phone_number = int(username)
            PhoneNumberField.run_validators(phone_number)
            user = User._default_manager.get_by_phone_number(phone_number)
            device_uuid = kwargs.get('device_uuid', None)

            if device_uuid is None:
                logger.error('phone_number: {0} device_uuid required'.format(user.phone_number))
                raise serializers.ValidationError(_('device_uuid required'))

            # checking if the user with this device_uuid is registered
            try:
                user.devices.get(device_uuid=device_uuid)
            except ObjectDoesNotExist:
                logger.error('phone_number: {0} this device does not registered'.format(user.phone_number))
                # raise serializers.ValidationError(_('This device does not registered'))
                return
            except MultipleObjectsReturned:
                logger.error('phone_number: {0} sequence contains more than one element'.format(user.phone_number))
                # raise serializers.ValidationError(_('The user has multiple device_uuid'))
                return

            if user.check_verify_code(password):
                user.removed_verify_code(password)
                return user
        except User.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            User().set_password(password)
        except (ValueError, ValidationError) as e:
            logger.error('SMS User Login Error: user -> {0}, detail -> {1}'.format(username, e))


class GoogleOAuthBackend(ModelBackend):

    def authenticate(self, username=None, password=None, **kwargs):
        logger.info('Google Auth Params is {0} and username is {1}'.format(kwargs, username))
        try:
            token_info = client.verify_id_token(password, None)
        except Exception as e:
            logger.error('Google Verify Exception: {0}'.format(e))
            return

        if username != token_info['email']:
            return

        try:
            user = User.objects.get(email=token_info['email'])
        except User.DoesNotExist:
            user = User.objects.create_user(
                email=token_info['email'],
                first_name=token_info.get('given_name', ''),
                last_name=token_info.get('family_name', ''),
            )
        except Exception as e:
            logger.error('Google User Login Error: user -> {0}, detail -> {1}'.format(username, e))
            return

        device_uuid = kwargs.get('device_uuid', None)
        if device_uuid:
            migrate_user(device_uuid, user, kwargs.get('notify_token', ''))

        return user


class VASBackend(ModelBackend):
    """
    VAS model backend for user verification
    Authenticates against settings.AUTH_USER_MODEL.
    """

    def authenticate(self, username=None, password=None, **kwargs):
        """
        VAS user verification

        :param username: the user phone number
        :param password: the user password

        :returns User: the authenticated user

        :raise ValidationError: if any exception raised { UserDevice does not exists,
        :raise UserDevice MultipleObjectsReturned, User DoesNotExist
        """

        # if pyapp_users.backends.VASBackend used then continue authenticating
        if pyapp_users_settings.VAS_SERVICE_ID is 0:
            return

        PhoneNumberField = User._meta.get_field('phone_number')
        try:
            phone_number = int(username)
            PhoneNumberField.run_validators(phone_number)
            user = User._default_manager.get_by_phone_number(phone_number)
            device_uuid = kwargs.get('device_uuid', None)

            if device_uuid is None:
                logger.error('phone_number: {0} device_uuid required'.format(user.phone_number))
                raise serializers.ValidationError(_('device_uuid required'))

            try:
                user_device = user.devices.get(device_uuid=device_uuid)
            except ObjectDoesNotExist:
                logger.error('phone_number: {0} this device does not registered'.format(user.phone_number))
                # raise serializers.ValidationError(_('This device does not registered'))
                return
            except MultipleObjectsReturned:
                logger.error('phone_number: {0} sequence contains more than one element'.format(user.phone_number))
                # raise serializers.ValidationError(_('The user has multiple device_uuid'))
                return

            # sending vas user verification request
            result = self.send_vas_user_verification(user_device.register_code, password)

            if result is None:
                return

            if device_uuid:
                migrate_user(device_uuid, user, kwargs.get('notify_token', ''))
            return user
        except User.DoesNotExist:
            logger.error('username: {0} user not found'.format(username))
            # raise serializers.ValidationError(_('User not found'))

    def send_vas_user_verification(self, register_code, verify_code):
        """
        sending vas user verification request

        :param register_code: user registration code
        :param verify_code: code that send

        :return json: returns data that contains user verification data

        :raises ValidationError: if verification failed
        """

        uri = pyapp_users_settings.VAS_VERIFY_URL.format(BASE_URL=pyapp_users_settings.VAS_BASE_URL,
                                                         REGISTER_CODE=register_code, VERIFY_CODE=verify_code)
        response = requests.get(uri, timeout=60)
        if response.ok:
            return response.json()
        else:
            logger.error('register_code: {0}'.format(register_code))
            logger.error('response error {0}'.format(response.text))
            return None
            # raise serializers.ValidationError(_('Verification failed: {0}'.format(response.text)))


class EmailBackend(ModelBackend):
    """
    Authenticates against settings.AUTH_USER_MODEL if email entered instead
    of username, so retrieve username from entered email and then authenticate
    """

    def authenticate(self, request, username=None, password=None, **kwargs):
        # Check if username is in email format or not
        try:
            validate_email(username)
            user = User._default_manager.get(email=username)
            username = user.username
        except ValidationError:
            # Entered input is not valid 'email'
            # format and probably is a 'username'
            pass
        except User.DoesNotExist:
            # No user associated with the given email
            return
        return super().authenticate(request, username, password, **kwargs)
