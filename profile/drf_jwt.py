from calendar import timegm
from datetime import timedelta, datetime

import jwt

from django.utils.translation import ugettext as _
import logging

from rest_framework import serializers
from rest_framework import exceptions

from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.views import JSONWebTokenAPIView
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.compat import PasswordField
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from django.contrib.auth import authenticate, get_user_model
from rest_framework_jwt.compat import Serializer

from .models import UserProfile, UserDevice
from .utils import jwt_decode_handler as custom_decoder, get_empty_uuid
from .token_operation import TokenOperation

User = get_user_model()
logger = logging.getLogger('pyapp.users')
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER


class MyJSONWebTokenSerializer(JSONWebTokenSerializer):
    """
    Serializer class used to validate a username and password.

    'username' is identified by the custom UserModel.USERNAME_FIELD.

    Returns a JSON Web Token that can be used to authenticate later calls.
    """
    def __init__(self, *args, **kwargs):
        """
        Dynamically add the USERNAME_FIELD to self.fields.
        """
        super(MyJSONWebTokenSerializer, self).__init__(*args, **kwargs)

        self.fields[self.username_field] = serializers.CharField()
        self.fields['password'] = PasswordField(write_only=True)
        self.fields['device_uuid'] = serializers.UUIDField(write_only=True,required=False)
        self.fields['notify_token'] = serializers.CharField(write_only=True, required=False, allow_blank=True)

    def validate(self, attrs):
        credentials = {
            self.username_field: attrs.get(self.username_field),
            'password': attrs.get('password'),
            'device_uuid': attrs.get('device_uuid'),
        }

        # For web based backend
        if attrs.get('device_uuid') is None:
            del credentials['device_uuid']

        if all(credentials.values()):
            credentials.update({'notify_token': attrs.get('notify_token', '')})
            user = authenticate(**credentials)
            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)

                # TEST REQUIRED: test with and without device_uuid
                device_uuid = get_empty_uuid()
                if credentials.get('device_uuid'):
                    device_uuid = credentials.get('device_uuid')

                token_operation = TokenOperation(user.id, device_uuid)

                if token_operation.current_user_device() is None:
                    UserDevice.objects.create(user=user, device_uuid=device_uuid, device_type=UserDevice.BROWSER)

                secret_key = TokenOperation.new_secret_key()
                payload = jwt_payload_handler(user, device_uuid, secret_key)

                token_operation.secret_key = secret_key

                return {
                    'token': jwt_encode_handler(payload),
                    'user': user,
                }
            else:
                msg = _('Unable to login with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "{username_field}" and "password".')
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)


class ObtainJSONWebToken(JSONWebTokenAPIView):
    """
    API View that receives a POST with a user's username and password.

    Returns a JSON Web Token that can be used for authenticated requests.
    """
    serializer_class = MyJSONWebTokenSerializer

    # TODO: update user.last_login datetime
    # def post(self, request, *args, **kwargs):
    #     print('ObtainJSONWebToken input data', request.data)
    #     return super(ObtainJSONWebToken, self).post(request, *args, **kwargs)


class MyJSONWebTokenAuthentication(JSONWebTokenAuthentication):

    def authenticate(self, request):
        """
        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication.  Otherwise returns `None`.
        """
        jwt_value = self.get_jwt_value(request)
        if jwt_value is None:
            return None

        try:
            payload = jwt_decode_handler(jwt_value)
        except jwt.ExpiredSignature:
            msg = _('Signature has expired.')
            raise exceptions.AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = _('Error decoding signature.')
            raise exceptions.AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise exceptions.AuthenticationFailed()

        user = self.authenticate_credentials(payload)

        return user, payload


class VerificationBaseSerializer(Serializer):
    """
    Abstract serializer used for verifying and refreshing JWTs.
    """
    token = serializers.CharField()
    token_operation = None

    def validate(self, attrs):
        msg = 'Please define a validate method.'
        raise NotImplementedError(msg)

    def _check_payload(self, token):
        # Check payload valid (based off of JSONWebTokenAuthentication,
        # may want to refactor)
        try:
            payload = jwt_decode_handler(token)
        except jwt.ExpiredSignature:
            # if an incoming token expired and the token has a valid secret key,
            # the token does refresh else raise a ValidationError for invalid token
            user_id, device_uuid = TokenOperation.get_user_info_from_token(token)
            self.token_operation = TokenOperation(user_id, device_uuid)
            if not self.token_operation.is_token_valid(token):
                logger.error('user_id: {0}, device_uuid: {1}: secret key is not valid'.format(
                             user_id, device_uuid))
                msg = _('The token is invalid')
                raise serializers.ValidationError(msg)

            payload = custom_decoder(token)
        except jwt.DecodeError:
            logger.warning('Error decoding signature')
            msg = _('Error decoding signature.')
            raise serializers.ValidationError(msg)

        else:
            # based our strategy, token must be refreshed only once
            # if an incoming token is valid, the token should not refresh
            msg = _('Token is still valid')
            raise serializers.ValidationError(msg)

        return payload

    def _check_user(self, payload):
        username = jwt_get_username_from_payload(payload)

        if not username:
            msg = _('Invalid payload.')
            raise serializers.ValidationError(msg)

        # Make sure user exists
        try:
            user = self.token_operation.current_user_device().user
        except User.DoesNotExist:
            logger.error('username: {0}: User doesn\'t exist.'.format(
                         username))
            msg = _("User doesn't exist.")
            raise serializers.ValidationError(msg)

        if not user.is_active:
            logger.error('user_id: {0}, username: {1}: User account is disabled.'.format(
                         user.id, user.username))
            msg = _('User account is disabled.')
            raise serializers.ValidationError(msg)

        return user


class RefreshTokenSerializer(VerificationBaseSerializer):
    """
    Refresh an access token.
    """

    def __init__(self, *args, **kwargs):
        """
        Dynamically add the USERNAME_FIELD to self.fields.
        """
        super(RefreshTokenSerializer, self).__init__(*args, **kwargs)

        self.fields['token'] = serializers.CharField(write_only=True)
        self.fields['device_uuid'] = serializers.UUIDField(write_only=True, required=False)

    def validate(self, attrs):
        token = attrs['token']
        device_uuid = attrs.get('device_uuid')

        if device_uuid is None:
            device_uuid = get_empty_uuid()

        payload = self._check_payload(token=token)
        user = self._check_user(payload=payload)
        # Get and check 'orig_iat'
        orig_iat = payload.get('orig_iat')

        if orig_iat:
            # Verify expiration
            refresh_limit = api_settings.JWT_REFRESH_EXPIRATION_DELTA

            if isinstance(refresh_limit, timedelta):
                refresh_limit = (refresh_limit.days * 24 * 3600 +
                                 refresh_limit.seconds)

            expiration_timestamp = orig_iat + int(refresh_limit)
            now_timestamp = timegm(datetime.utcnow().utctimetuple())

            if now_timestamp > expiration_timestamp:
                msg = _('Refresh has expired.')
                raise serializers.ValidationError(msg)
        else:
            logger.error('user_id: {0}, device_uuid: {1}: orig_iat field is required.'.format(
                         user.id, device_uuid))
            msg = _('orig_iat field is required.')
            raise serializers.ValidationError(msg)

        # FIXED: get new uuid from TokenOperation: remove dependency to uuid
        # FIXED: in this class and other classes
        secret_key = TokenOperation.new_secret_key()
        new_payload = jwt_payload_handler(user, device_uuid, secret_key)
        new_payload['orig_iat'] = orig_iat

        # self.token_operation = TokenOperation(user.id, device_uuid)
        self.token_operation.secret_key = secret_key

        return {
            'token': jwt_encode_handler(new_payload),
            'user': user
        }


class RefreshJSONWebToken(JSONWebTokenAPIView):
    """
    API View that returns a refreshed token (with new expiration) based on
    existing token

    If 'orig_iat' field (original issued-at-time) is found, will first check
    if it's within expiration window, then copy it to the new token
    """
    serializer_class = RefreshTokenSerializer


obtain_jwt_token = ObtainJSONWebToken.as_view()
refresh_jwt_token = RefreshJSONWebToken.as_view()

