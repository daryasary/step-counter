import random
from datetime import timedelta

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core import validators
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager, send_mail


class AggregateRoot(models.Model):
    """
    base model for all models
    """
    created_date = models.DateTimeField(_('creation on'), auto_now_add=True)
    modified_date = models.DateTimeField(_('modified on'), auto_now=True)

    class Meta:
        # This model will then not be used to create any database table. Instead, when it is used as a base class for
        # other models, its fields will be added to those of the child class.
        abstract = True


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, phone_number, email, password, is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        user = self.model(phone_number=phone_number,
                          username=username, email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username=None, phone_number=None, email=None, password=None, **extra_fields):
        if username is None:
            if email:
                username = email.split('@', 1)[0]
            if phone_number:
                username = random.choice('abcdefghijklmnopqrstuvwxyz') + str(phone_number)[-7:]
            while User.objects.filter(username=username).exists():
                username += str(random.randint(10, 99))

        return self._create_user(username, phone_number, email, password, False, False, **extra_fields)

    def create_superuser(self, username, phone_number, email, password, **extra_fields):
        return self._create_user(username, phone_number, email, password, True, True, **extra_fields)

    def get_by_phone_number(self, phone_number):
        return self.get(**{'phone_number': phone_number})


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """
    username = models.CharField(_('username'), max_length=32, unique=True,
        help_text=_('Required. 30 characters or fewer starting with a letter. Letters, digits and underscore only.'),
        validators=[
            validators.RegexValidator(r'^[a-zA-Z][a-zA-Z0-9_\.]+$',
                                      _('Enter a valid username starting with a-z. '
                                        'This value may contain only letters, numbers '
                                        'and underscore characters.'), 'invalid'),
        ],
        error_messages={
            'unique': _("A user with that username already exists."),
        }
    )
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), unique=True, null=True, blank=True)
    phone_number = models.BigIntegerField(_('mobile number'), unique=True, null=True, blank=True,
        validators=[
            # TODO: use utils.validators
            validators.RegexValidator(r'^989[0-3,9]\d{8}$',
                                     _('Enter a valid mobile number.'), 'invalid'),
        ],
        error_messages={
            'unique': _("A user with this mobile number already exists."),
        }
    )
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as active. '
                    'Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    # verify_codes = models.CommaSeparatedIntegerField(_('verification codes'), max_length=30, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'phone_number']

    class Meta:
        db_table = 'auth_user'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    @property
    def is_loggedin_user(self):
        """
        Returns True if user has actually logged in with valid credentials.
        """
        return self.phone_number is not None or self.email is not None

    def check_verify_code(self, verify_code):
        return self.verification_codes.\
                        filter(verification_code=verify_code).\
                        filter(created_time__gt=timezone.now() - timedelta(hours=6)).first()

    def set_verify_code(self, verify_code):
        self.verification_codes.create(verification_code=verify_code)

    def removed_verify_code(self, verify_code):
        self.verification_codes.\
            filter(verification_code=verify_code).\
            delete()

    def save(self, *args, **kwargs):
        if self.email is not None and self.email.strip() == '':
            self.email = None
        super().save(*args, **kwargs)

    def is_email_verified(self, email):
        return self.email.lower() == email.lower()


class VerifyCode(models.Model):
    created_time = models.DateTimeField(_('creation time'), auto_now_add=True)
    user = models.ForeignKey(User, verbose_name=_('user'), related_name='verification_codes')
    verification_code = models.PositiveIntegerField(_('verification code'))

    class Meta:
        db_table = 'auth_user_verification_code'
        index_together = ['user', 'verification_code']


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nick_name = models.CharField(_('nick_name'), max_length=150, blank=True)
    avatar = models.CharField(_('avatar'), blank=True, max_length=32)
    birthday = models.DateField(_('birthday'), null=True, blank=True)
    gender = models.NullBooleanField(_('gender'), help_text=_('female is False, male is True, null is unset'))
    phone_number = models.BigIntegerField(_('mobile number'), null=True, blank=True,
        validators=[
            validators.RegexValidator(r'^989[0-3,9]\d{8}$',
                                     _('Enter a valid mobile number.'), 'invalid'),
        ],
    )
    step_count = models.IntegerField(_("step count"), default=0)

    class Meta:
        db_table = 'auth_profiles'
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')


class UserDevice(models.Model):
    MOBILE = '1'
    BROWSER = '2'

    DEVICE_TYPE = (
        (MOBILE, 'Mobile'),
        (BROWSER, 'Browser')
    )

    user = models.ForeignKey(User, related_name='devices')
    device_uuid = models.UUIDField(_('device UUID'))
    secret_key = models.UUIDField(_('secret key'), null=True, blank=True)
    notify_token = models.CharField(_('notification token'), max_length=50, null=True, blank=True)
    created_time = models.DateTimeField(_('creation on'), auto_now_add=True, db_index=True)
    updated_time = models.DateTimeField(_('modified on'), auto_now=True)
    device_model = models.CharField(_('device model'), max_length=150, null=True, blank=True)
    device_os = models.CharField(_('device OS'), max_length=150, null=True, blank=True)
    register_code = models.PositiveIntegerField(_('vas register code'), null=True, blank=True, unique=True)
    device_type = models.CharField(_('device type'), max_length=2, choices=DEVICE_TYPE, default=MOBILE)

    class Meta:
        db_table = 'auth_devices'
        verbose_name = _('device')
        verbose_name_plural = _('devices')
        unique_together = ('user', 'device_uuid')

    def __str__(self):
        return '{0}: {1}'.format(self.user.username, self.device_model)


class UserEmail(AggregateRoot):
    """
    User emails
    """
    user = models.ForeignKey(User, related_name='emails')
    new_email = models.EmailField(_('new email'), db_index=True)
    old_email = models.EmailField(_('old email'), null=True, blank=True)
    verify_code = models.PositiveIntegerField(_('verification code'))
    verify_time = models.DateTimeField(_('verification time'), null=True)

    class Meta:
        db_table = 'auth_user_email'
        verbose_name = _('user_email')
        verbose_name_plural = _('user_emails')

    def __str__(self):
        return '{0}: old email {1} replaced/ing with new email {2}'\
            .format(self.modified_date, self.new_email, self.old_email)

    def is_email_verified(self):
        return self.verify_time is not None


class UserPhoneNumber(AggregateRoot):
    """
    User phone numbers
    """
    user = models.ForeignKey(User, related_name='phone_numbers')
    new_phone_number = models.BigIntegerField(_('new phone number'), db_index=True,
                                          validators=[
                                              validators.RegexValidator(r'^989[0-3,9]\d{8}$',
                                                                        _('Enter a valid mobile number.'), 'invalid'),
                                          ],
                                          )
    old_phone_number = models.BigIntegerField(_('old phone number'), null=True, blank=True,
                                          validators=[
                                              validators.RegexValidator(r'^989[0-3,9]\d{8}$',
                                                                        _('Enter a valid mobile number.'), 'invalid'),
                                          ],
                                          )
    verify_code = models.PositiveIntegerField(_('verification code'))
    verify_time = models.DateTimeField(_('verification time'), null=True)

    class Meta:
        db_table = 'auth_user_phone_number'
        verbose_name = _('user_phone_number')
        verbose_name_plural = _('user_phone_numbers')

    def __str__(self):
        return '{0}: old phone number {1} replaced/ing with new phone number {2}'\
            .format(self.modified_date, self.new_phone_number, self.old_phone_number)

    def is_phone_number_verified(self):
        return self.verify_time is not None
