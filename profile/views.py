import logging

from django.contrib.auth import get_user_model
from django.utils.timezone import datetime
from django.utils.translation import ugettext_lazy as _


from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from rest_framework.throttling import AnonRateThrottle, UserRateThrottle

from profile.permissions import TimeIsAllowed
from .serializers import ProfileSerializer, UserRegistrationSerializer, UpdateEmailSerializer, UpdatePhoneNumberSerializer, \
    UserSerializer, UpdateStepSerializer, LeaderBoardSerializer
from .models import UserProfile, UserDevice, UserEmail, UserPhoneNumber

User = get_user_model()
logger = logging.getLogger('pyapp.users')


class ProfileAPIView(generics.RetrieveUpdateAPIView):
    """
    GET or UPDATE user profile info such as first name, last name, gender, birthday, ...
    """
    serializer_class = ProfileSerializer
    queryset = UserProfile.objects.select_related('user').all()
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        user = self.request.user
        obj, created = UserProfile.objects.get_or_create(
            user=user,
            defaults={
                'nick_name': user.first_name,
                'phone_number': user.phone_number,
            }
        )
        return obj


class UserRegistrationAPIView(generics.CreateAPIView):
    """
    User registration with phone number
    """
    serializer_class = UserRegistrationSerializer
    throttle_classes = (AnonRateThrottle, )
    queryset = UserDevice.objects.all()


class UpdateEmailAPIView(generics.CreateAPIView):
    """
    Adding and substitute user email addresses django rest framework CreateAPIView
    """
    serializer_class = UpdateEmailSerializer
    queryset = UserEmail.objects.all()
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, )
    throttle_classes = (UserRateThrottle, )


class VerifyUserEmailAPIView(generics.RetrieveAPIView):
    """
    Verify new email: django rest framework RetrieveAPIView
    """
    serializer_class = UpdateEmailSerializer
    queryset = UserEmail.objects.all()
    throttle_classes = (AnonRateThrottle, )

    def retrieve(self, request, *args, **kwargs):
        """
        We use this method for verify email addresses
        :param request: django rest framework Request
        :param args: default parameter
        :param kwargs: default parameter
        :return response: django rest framework Response
        """
        verify_code = self.request.query_params.get('code')
        email = self.request.query_params.get('email')
        user_email = self.queryset.filter(new_email=email).filter(verify_code=verify_code).first()

        if user_email is None or user_email.is_email_verified():
            raise ValidationError({'detail': _('verification failed')})

        user_email.verify_time = datetime.now()
        # substitute user email to the verified email
        user_email.user.email = email
        user_email.save()
        user_email.user.save()
        user_serializer = self.serializer_class(user_email)
        return Response(user_serializer.data, status=200)


class UpdatePhoneNumberAPIView(generics.CreateAPIView):
    """
    Adding and substitute user phone number django rest framework CreateAPIView
    """
    serializer_class = UpdatePhoneNumberSerializer
    queryset = UserPhoneNumber.objects.all()
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, )
    throttle_classes = (UserRateThrottle, )


class VerifyUserPhoneNumberAPIView(generics.RetrieveAPIView):
    """
    Verify new phone number: django rest framework RetrieveAPIView
    """
    serializer_class = UpdatePhoneNumberSerializer
    queryset = UserPhoneNumber.objects.all()
    throttle_classes = (AnonRateThrottle, )

    def retrieve(self, request, *args, **kwargs):
        """
        We use this method for verify email addresses
        :param request: django rest framework Request
        :param args: default parameter
        :param kwargs: default parameter
        :return response: django rest framework Response
        """
        verify_code = self.request.query_params.get('code')
        phone_number = self.request.query_params.get('phone_number')
        user_phone_number = self.queryset.filter(new_phone_number=phone_number).filter(verify_code=verify_code).first()

        if user_phone_number is None or user_phone_number.is_phone_number_verified():
            raise ValidationError({'detail': _('verification failed')})

        user_phone_number.verify_time = datetime.now()
        # substitute user phone to the verified phone
        user_phone_number.user.phone_number = phone_number
        # user_phone_number.user.set_verify_code(verify_code)
        user_phone_number.save()
        user_phone_number.user.save()
        user_serializer = self.serializer_class(user_phone_number)
        return Response(user_serializer.data, status=200)


class UserCreationAPIView(generics.CreateAPIView):
    """Register user by provided username, password
    and email instead of phone_number"""
    serializer_class = UserSerializer
    queryset = User.objects.all()
    throttle_classes = (AnonRateThrottle, )


class StepUpdateAPIView(generics.UpdateAPIView):
    """Register user by provided username, password
    and email instead of phone_number"""
    serializer_class = UpdateStepSerializer
    queryset = UserProfile.objects.all()
    throttle_classes = (UserRateThrottle,)
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, TimeIsAllowed)

    def get_object(self):
        return self.request.user.userprofile


class LeaderBoardAPIView(generics.ListAPIView):
    serializer_class = LeaderBoardSerializer
    queryset = UserProfile.objects.all().order_by('-step_count')
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, TimeIsAllowed)

    def get_queryset(self):
        queryset = list(self.queryset[:10])
        if self.request.user.is_authenticated():
            queryset.append(self.queryset.filter(user=self.request.user).first())
        return queryset
