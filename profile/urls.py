from django.conf.urls import url

from .drf_jwt import obtain_jwt_token, refresh_jwt_token
from .views import ProfileAPIView, UserRegistrationAPIView, UpdateEmailAPIView,\
    VerifyUserEmailAPIView, UpdatePhoneNumberAPIView, VerifyUserPhoneNumberAPIView, \
    UserCreationAPIView, StepUpdateAPIView, LeaderBoardAPIView

urlpatterns = [
    url(r'^register/$', UserRegistrationAPIView.as_view(), name='register'),
    url(r'^register-user/$', UserCreationAPIView.as_view(), name='register-user'),
    url(r'^obtain-token/', obtain_jwt_token, name='obtain-token'),
    url(r'^refresh-token/', refresh_jwt_token, name='refresh-token'),
    url(r'^profile/$', ProfileAPIView.as_view(), name='profile-detail'),
    url(r'^update-email/$', UpdateEmailAPIView.as_view(), name='add-email'),
    url(r'^verify-email/$', VerifyUserEmailAPIView.as_view(), name='verify-email'),
    url(r'^update-phone-number/$', UpdatePhoneNumberAPIView.as_view(), name='add-phone-number'),
    url(r'^verify-phone-number/$', VerifyUserPhoneNumberAPIView.as_view(), name='verify-phone-number'),
    url(r'^update-steps/$', StepUpdateAPIView.as_view(), name='update_steps'),
    url(r'^leader-board/$', LeaderBoardAPIView.as_view(), name='leader-board'),
]
