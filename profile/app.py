from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UserAppConfig(AppConfig):
    name = 'profile'
    verbose_name = _('profile')
