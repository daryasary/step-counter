import logging
import base64
import hashlib
import jwt
import requests
from django.template.loader import render_to_string
from django.conf import settings
from rest_framework.exceptions import ValidationError, AuthenticationFailed
from rest_framework_jwt.settings import api_settings
from Crypto import Random
from Crypto.Cipher import AES
from rest_framework_jwt.utils import jwt_payload_handler

from profile.settings import pyapp_users_settings
logger = logging.getLogger('pyapp.users')


def send_vas_verification(data):
    """
    sending user registration data to vas service

    :param data: dict contains user registration data

    :return json: Returns data that contains user registration data

    :raise ValidationError: if registration failed
    """
    uri = pyapp_users_settings.VAS_REGISTER_URL.format(BASE_URL=pyapp_users_settings.VAS_BASE_URL,
                                                       SERVICE_ID=pyapp_users_settings.VAS_SERVICE_ID)
    logger.info('vas uri generated {0}'.format(uri))
    response = requests.post(uri, data=data, timeout=60)

    if response.ok:
        return response.json()
    else:
        logger.error('phone_number: {0}, device_uuid: {1}'.format(data.get('phone_number'),
                                                                  data.get('device_uuid')))
        logger.error('response error {0}'.format(response.text))
        return None


def jwt_decode_handler(token):
    """
    custom jwt decoder that ignore verify_exp
    :param token: the incoming token

    :returns payload: a dict contains user claims
    """
    options = {
        'verify_exp': False,
    }

    return jwt.decode(
        token,
        api_settings.JWT_SECRET_KEY,
        api_settings.JWT_VERIFY,
        options=options,
        leeway=api_settings.JWT_LEEWAY,
        audience=api_settings.JWT_AUDIENCE,
        issuer=api_settings.JWT_ISSUER,
        algorithms=[api_settings.JWT_ALGORITHM]
    )


def jwt_response_payload_handler(token, user=None, request=None):
    """
    customize jwt JWT_RESPONSE_PAYLOAD_HANDLER for our purpose
    :param token: the user token
    :param user: the user info
    :param request: the incoming request
    :return dict: a dict contains token and fino_token(gateway token)
    """
    pay_auth_token = ''
    if user.phone_number and pyapp_users_settings.FINOTECH_SERVICE_ID:
        try:
            data_params = {
                'phone_number': user.phone_number,
                'service': pyapp_users_settings.FINOTECH_SERVICE_ID,
                'device_uuid': request.data['device_uuid'],
                'device_model': '',
                'device_os': '',
            }
            response = requests.post(pyapp_users_settings.FINOTECH_TOKEN_URL, data=data_params)
            response.raise_for_status()

            pay_auth_token = response.json().get('token', '')
        except Exception as e:
            logger.error('fino token error: {0}'.format(e))

    return {
        'token': token,
        'fino_token': pay_auth_token,
    }


def jwt_payload_extender(user, device_uuid, secret_key):
    """
    extending payload handler for adding device uuid and secret key to the payload
    :param user: the user info
    :param device_uuid: user device uuid
    :param secret_key: the secret key for refresh_token operation
    :return payload: the extended payload
    """
    payload = jwt_payload_handler(user)
    payload['device_uuid'] = str(device_uuid)
    payload['secret_key'] = str(secret_key)
    return payload


def get_empty_uuid():
    """
    return an empty uuid
    :return str: a string that contain zero with uuid format
    """
    return '00000000-0000-0000-0000-000000000000'


def get_verification_function():
    """
    retrieve verification method from string
    :return func: a retrieved function from string
    """
    import importlib
    function_string = pyapp_users_settings.SMS_VERIFICATION_HANDLER
    module_name, func_name = function_string.rsplit('.', 1)
    module = importlib.import_module(module_name)
    func = getattr(module, func_name)
    return func


def get_encryption_class():
    """
    retrieve encryption class from string
    :return class: a retrieved encryption from string
    """
    import importlib
    function_string = pyapp_users_settings.ENCRYPTION_CLASS
    module_name, func_name = function_string.rsplit('.', 1)
    module = importlib.import_module(module_name)
    cls = getattr(module, func_name)
    return cls


def send_verification(phone_number, verify_code):
    """
    sending verification code to the phone number

    :param phone_number: the user phone number
    :param verify_code: the verification code

    :raise ValidationError: if sending a message failed
    """

    # message = render_to_string(settings.VERIFICATION_TEMPLATE_PATH, {
    #     'verify_code': verify_code
    # })

    message = 'کد فعالسازی: {}'.format(verify_code)
    query_params = {
        'username': settings.VERIFICATION_USERNAME,
        'password': settings.VERIFICATION_PASSWORD,
        'srcaddress': settings.VERIFICATION_SMS_NUMBER,
        'dstaddress': phone_number,
        'body': message,
        'unicode': settings.UNICODE,
    }

    try:
        response = requests.get(settings.VERIFICATION_URI,
                                params=query_params, timeout=60)

        response.raise_for_status()

        if response.ok and 'ERR' in response.text:
            logger.error('ADP verification failed: {0}'.format(query_params))
            raise AuthenticationFailed(detail='Server authentication failed')

    except Exception as e:
        logger.error('Sending verification code to {0} failed: {1}'.format(phone_number, e))
        raise ValidationError({'detail': 'Could not sent SMS verification'})


class AESCipher(object):
    """
    this class is used for encryption and decryption based on AES
    :attr length: length of AES block size
    :attr key: a key for encryption and decryption operations
    """

    def __init__(self, key):
        self.length = 16
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        """
        this method get a raw string and encrypt it with AES CBC mode
        :param raw: a string that we want to encrypt
        :return: a encrypted string
        """
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)  # AES block_size is 16
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        """
        this method get a encoded string and decode it
        :param enc: a string that encoded with AES CBC mode
        :return: a decoded string
        """
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, text):
        """
        add padding to a text
        in this state we add a padding to a incoming text, padding character is a hex character of padding length
        :param text: a string that we want to add padding to end of it
        :return: the incoming string that contains padding
        """
        return text + (self.length - len(text) % self.length) * chr(self.length - len(text) % self.length)

    @staticmethod
    def _unpad(text):
        """
        remove padding for text
        :param text: a string that we want to remove padding from end of it
        :return: the incoming string that removed padding
        """
        return text[:-ord(text[len(text) - 1:])]
